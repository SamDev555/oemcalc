// Storage Controller
const StorageController = (function() {

    return {
        storeProducts : function (product) {
            let products;
            if(localStorage.getItem('products') == null){
                products = [];
                products.push(product);
            }else{
                products = JSON.parse(localStorage.getItem('products'));
                products.push(product);
            }
            localStorage.setItem('products',JSON.stringify(products));
        },
        getProducts : function () {
            let products;

            if(localStorage.getItem('products') == null){
                products = [];
            }else{
                products = JSON.parse(localStorage.getItem('products'));
            }
            return products;
        },
        updateProduct : function (product) {
            let products = JSON.parse(localStorage.getItem('products'));

            products.forEach(function (prd,index){
                if(prd.id == product.id){
                    products.splice(index,1,product);
                }
            });
            localStorage.setItem('products',JSON.stringify(products));
        },
        deleteProduct : function (id) {
            let products = JSON.parse(localStorage.getItem('products'));
            products.forEach(function (prd,index) {
                if(id == prd.id){
                    products.splice(index,1)
                }
            });
            localStorage.setItem('products',JSON.stringify(products));
        }
    }
})();

// Product Controller
const ProductController = (function() {
    // private
    function Product(id,name,price){
        this.id = id;
        this.name = name;
        this.price = price;
    }
    const data = {
        products: StorageController.getProducts(),
        selectedProduct: null,
        totalPrice: 0
    };

    // public
    return {
        getProducts: function(){
            return data.products;
        },
        getData: function(){
            return data
        },
        addProduct : function (name,price) {
            let id;
            if(data.products.length > 0){
                id = data.products[data.products.length - 1].id + 1;
            }else{
                id = 0;
            }
            const newProduct  = new Product(id,name,parseFloat(price));
            data.products.push(newProduct);
            return newProduct;
        },
        updateProduct : function(name,price){
            let product = null;
            data.products.forEach(function (prd) {
                if(prd.id == data.selectedProduct.id){
                    prd.name = name;
                    prd.price = parseFloat(price);
                    product = prd;
                }
            });
            return product;
        },
        deleteProduct : function(product){
            data.products.forEach(function (prd,index) {
                if(prd.id == product.id){
                    data.products.splice(index,1);
                }
            });
            return data.products;
        },
        getTotal : function () {
            let total = 0;
            data.products.forEach(function (item) {
                total += item.price;
            });
            data.totalPrice = total;
            return data.totalPrice;
        },
        getProductById : function (id) {
            let product  = null;
            data.products.forEach(function (prd) {
                if(prd.id == id){
                   product = prd
                }
            });
            return product;
        },
        setCurrentProduct : function (selectedPrd) {
            data.selectedProduct = selectedPrd;
        },
        getCurrentProduct : function () {
            return data.selectedProduct;
        }
    }
})();

// UI controller 
const UIController = (function() {
    // private
    const Selectors = {
        selectList : '#item-list',
        addButton : '.addBtn',
        productListItems : '#item-list tr',
        updateButton : '.updateBtn',
        deleteButton : '.deleteBtn',
        cancelButton : '.cancelBtn',
        productName : '#productName',
        productPrice : '#productPrice',
        productCard : '#productCard',
        totalAzn : '#total-azn',
        totalDlr : '#total-dlr'
    };
    // public
    return {
        createProductList: function(products){
            let html = '';
            products.forEach(prd => {
                html += `
                <tr>
                    <td>${prd.id}</td>
                    <td>${prd.name}</td>
                    <td>${prd.price}</td>
                    <td class="text-right">
                        <i class="far fa-edit edit-product"></i>
                    </td>
                </tr>       
                `;
            });
            document.querySelector(Selectors.selectList).innerHTML = html;
        },
        getSelectors: function(){
            return Selectors;
        },
        addProductToList: function (prd) {
            document.querySelector(Selectors.productCard).style.display = 'block';
            let item = `
                 <tr>
                    <td>${prd.id}</td>
                    <td>${prd.name}</td>
                    <td>${prd.price} $</td>
                    <td class="text-right">
                        <i class="far fa-edit edit-product"></i>
                    </td>
                </tr>      
            `;
            document.querySelector(Selectors.selectList).innerHTML +=item;
        },
        deleteProduct: function(){
            let items = document.querySelectorAll(Selectors.productListItems);
            items.forEach(function (item) {
                if(item.classList.contains('bg-warning')){
                    item.remove();
                }
            })
        },
        updateProduct : function(prd){
            let updatedItem = null;
            let items = document.querySelectorAll(Selectors.productListItems);
            items.forEach(function (item){
                if(item.classList.contains('bg-warning')){
                    item.children[1].textContent = prd.name;
                    item.children[2].textContent = prd.price + ' $';
                }
                updatedItem = item;
            });
            return updatedItem;
        },
        clearInputs: function () {
            document.querySelector(Selectors.productName).value = '';
            document.querySelector(Selectors.productPrice).value = '';
        },
        clearWarning : function(){
          const items = document.querySelectorAll(Selectors.productListItems);
          items.forEach(function (item) {
              if(item.classList.contains('bg-warning')){
                  item.classList.remove('bg-warning');
              }
          })
        },
        hideCard : function () {
            document.querySelector(Selectors.productCard).style.display = 'none';
        },
        showTotal : function (total) {
            document.querySelector(Selectors.totalDlr).textContent = total.toFixed(2);
            document.querySelector(Selectors.totalAzn).textContent = (total*1.7).toFixed(2);

        },
        addProductToForm : function () {
            const currentProduct = ProductController.getCurrentProduct();
            document.querySelector(Selectors.productName).value = currentProduct.name;
            document.querySelector(Selectors.productPrice).value = currentProduct.price;
        },
        addingState : function () {
            UIController.clearInputs();
            UIController.clearWarning();
            document.querySelector(Selectors.addButton).style.display = 'inline';
            document.querySelector(Selectors.updateButton).style.display = 'none';
            document.querySelector(Selectors.deleteButton).style.display = 'none';
            document.querySelector(Selectors.cancelButton).style.display = 'none';
        },
        editState : function (elem){
            elem.classList.add('bg-warning');
            document.querySelector(Selectors.addButton).style.display = 'none';
            document.querySelector(Selectors.updateButton).style.display = 'inline';
            document.querySelector(Selectors.deleteButton).style.display = 'inline';
            document.querySelector(Selectors.cancelButton).style.display = 'inline';
        }
    }
})();

// App controller 
const AppController = (function(ProductCtrl,UICtrl,StorageCtrl) {
    
    // getting selectors from UIController
    const Selectors = UICtrl.getSelectors();
    // loading event listeners
    const loadEventListeners = function(){
        document.querySelector(Selectors.addButton).addEventListener('click',productAddSubmit);
        document.querySelector(Selectors.selectList).addEventListener('click',productEditClick);
        document.querySelector(Selectors.updateButton).addEventListener('click',productEditSubmit);
        document.querySelector(Selectors.cancelButton).addEventListener('click',cancelUpdate);
        document.querySelector(Selectors.deleteButton).addEventListener('click',deleteProductSubmit);
    };
    
    const productAddSubmit = function(e){
        const productName = document.querySelector(Selectors.productName).value;
        const productPrice = document.querySelector(Selectors.productPrice).value;

        if(productName !=='' && productPrice !== ''){
            // Add product
           const newProduct = ProductCtrl.addProduct(productName,productPrice);
            // add item to list
           UICtrl.addProductToList(newProduct);
            // add item to LS
            StorageCtrl.storeProducts(newProduct);
            // get total
            const total = ProductCtrl.getTotal();
            // show total
            UICtrl.showTotal(total);
            // clear input value
            UICtrl.clearInputs();
        }
        e.preventDefault();
    };

    const productEditSubmit = function (e) {
        const productName = document.querySelector(Selectors.productName).value;
        const productPrice = document.querySelector(Selectors.productPrice).value;
        if(productName !== '' && productPrice !== ''){
            // update product
            let updatedProduct = ProductCtrl.updateProduct(productName,productPrice);
            // update UI
            let item = UICtrl.updateProduct(updatedProduct);
            // get total
            const total = ProductCtrl.getTotal();
            // show total
            UICtrl.showTotal(total);
            // update LS
            StorageCtrl.updateProduct(updatedProduct);
            // for removing bg-warning after save changes
            UICtrl.addingState();

        }

        e.preventDefault();
    };

    const productEditClick = function (e) {
        if(e.target.classList.contains('edit-product')){
          const id = e.target.parentElement.previousElementSibling.previousElementSibling.previousElementSibling.textContent;
          // get selected element
          const product =  ProductCtrl.getProductById(id);
            // set selected element
                ProductCtrl.setCurrentProduct(product);

                UICtrl.clearWarning();

            // add selected product to Form
                UICtrl.addProductToForm(product);
            // edit state
            UICtrl.editState(e.target.parentNode.parentNode);
        }
        e.preventDefault();
    };

    const cancelUpdate = function (e) {
        console.log('cancel clicked');


        UICtrl.addingState();
        e.preventDefault();
    };

    const deleteProductSubmit = function (e) {
        console.log('deleted');
        // get selected product
        const selectedProduct = ProductCtrl.getCurrentProduct();
        // delete selected product
        ProductController.deleteProduct(selectedProduct);
        // delete UI
        UICtrl.deleteProduct();
        // get total
        const total = ProductCtrl.getTotal();
        // show total
        UICtrl.showTotal(total);
        // delete form LS
        StorageCtrl.deleteProduct(selectedProduct.id);
        // adding state
        UICtrl.addingState();
        // hiding null list
        const products = ProductCtrl.getProducts();
        if(products.length == 0){
            UICtrl.hideCard();
        }
        e.preventDefault();
    };

    return {
        init: function(){
            console.log('starting app');

            UICtrl.addingState();

            let products = ProductCtrl.getProducts();

            if(products.length == 0){
                UICtrl.hideCard();
            }else{
                UICtrl.createProductList(products);
                // get total
                let total = ProductCtrl.getTotal();
                // set total
                UICtrl.showTotal(total);
            }
            // load eventListeners...
            loadEventListeners();
        }
    }
})(ProductController,UIController,StorageController);

// Starting program through AppController
AppController.init();
